import init, { Searcher as RustTpfServer } from "https://claudius.teodorescu.gitlab.io/triple-pattern-fragments-server/tpf_server.js";

// initializations
await init();

export default class TpfServer {
    constructor(hdtArchive) {
        return this.init(hdtArchive);
    }

    async init(hdtArchive) {
        for (const file of hdtArchive) {
            let file_name = file.name;
            let file_contents = await file.blob.arrayBuffer();
            file_contents = new Uint8Array(file_contents);

            switch (file_name) {
                case "subjects_section.fst":
                    this._subjects_searcher = new RustTpfServer(file_contents);
                    break;
                case "predicates_section.fst":
                    this._predicates_searcher = new RustTpfServer(file_contents);
                    break;
                case "objects_section.fst":
                    this._objects_searcher = new RustTpfServer(file_contents);
                    break;
                case "id_triples.fst":
                    this._id_triples_searcher = new RustTpfServer(file_contents);
                    break;
            }
        };

        return this;
    }

    locate_objects = (selector) => {
        return this._objects_searcher.locate(selector);
    }
}