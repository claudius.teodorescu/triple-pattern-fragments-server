# Triple Pattern Fragments Server

## Description
This module is designated to act as a server for Triple Pattern Fragments (see [Triple Pattern Fragments
A low-cost, queryable Linked Data Fragments interface](https://linkeddatafragments.org/specification/triple-pattern-fragments)).

## Scope
This module loads an RDF dataset and allows querying of the dataset by using ??.

## Technology stack
The module is currently written in Javascript and Rust compiled to WebAssembly, but it will be eventually be written completely in Rust and compiled to WebAssembly. Maybe, in the future, it will be helpful to have the module be called from a Javascript service worker (with Request / Response), with the dataset located according to (File and Directory Entries API)[https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API], in order to simulate a server for static websites.

## APIs

### Ideas: https://github.com/doriantaylor/xslt-rdfa?tab=readme-ov-file#programming-interface

### triple-pattern-fragment(triple-pattern)

This function is to be called as part o an execution plan for a SPARQL query.
### locate_by_exact_form("subjects|predicates|objects", value)
### locate_by_regex("subjects|predicates|objects", value)
### locate_by_prefix("subjects|predicates|objects", value)
### locate_by_ngram("subjects|predicates|objects", value)
### extract("subjects|predicates|objects", id)
### translate(triple-pattern-fragment)

get-all-subjects/predicates/objects()
get-subjects/predicates/objects()
For the last ones, the arguments can also be functions for search (filtering?).

## For the future: Functions allowed in triple pattern, in any position
### exact("term")
### prefix("prefix")
### regex("regex")
### ngram(?object, "token")
### o-max("token_1", "token_2", 3)
For proximity search: returns the triples containing tokens located in a document at a maximum distance of three tokens between them, in the order of the function's arguments.
### u-max("token_1", "token_2", 3)
For proximity search: returns the triples containing tokens located in a document at a maximum distance of three tokens between them, in any order.
### o-exact("token_1", "token_2", 3)
For proximity search: returns the triples containing tokens located in a document at an exact distance of three tokens between them, in the order of the function's arguments.
### u-max("token_1", "token_2", 3)
For proximity search: returns the triples containing tokens located in a document at an exact distance of three tokens between them, in any order.

## Various resources
https://docs.rs/fst/latest/fst/raw/struct.Fst.html
https://github.com/wilsonzlin/edgesearch
