use fst::automaton;
use fst::{Automaton, IntoStreamer, Streamer};
use regex_automata::dense;
use wasm_bindgen::prelude::*;
use wasm_bindgen_test::*;

#[wasm_bindgen]
pub struct TpfServer {
    map: fst::Map<Vec<u8>>,
}

#[wasm_bindgen]
impl TpfServer {
    #[wasm_bindgen(constructor)]
    pub fn new(data: Vec<u8>) -> TpfServer {
        let map = fst::Map::new(data).unwrap();

        TpfServer { map }
    }

    pub fn prefix_search(&mut self, query: &str) -> Result<js_sys::Map, JsError> {
        let prefix = automaton::Str::new(query).starts_with();

        let stream = &mut self.map.search(prefix).into_stream();

        let results = js_sys::Map::new();
        while let Some((k, v)) = stream.next() {
            results.set(
                &wasm_bindgen::JsValue::from(std::str::from_utf8(k).unwrap()),
                &wasm_bindgen::JsValue::from(v.to_string()),
            );
        }

        Ok(results)
    }

    pub fn regex_search(&mut self, query: &str) -> Result<js_sys::Map, JsError> {
        let formatted_query = format!(r"(?i){}", query);
        let dfa = dense::Builder::new()
            .anchored(true)
            .build(formatted_query.as_str())
            .unwrap();

        let stream = &mut self.map.search(&dfa).into_stream();
        // .into_str_vec().unwrap()

        let results = js_sys::Map::new();
        while let Some((k, v)) = stream.next() {
            results.set(
                &wasm_bindgen::JsValue::from(std::str::from_utf8(k).unwrap()),
                &wasm_bindgen::JsValue::from(v.to_string()),
            );
        }

        Ok(results)
    }

    pub fn levenstein_1_search(&mut self, query: &str) -> Result<js_sys::Map, JsError> {
        let levenstein = automaton::Levenshtein::new(query, 1)?;

        let stream = &mut self.map.search(levenstein).into_stream();

        let results = js_sys::Map::new();
        while let Some((k, v)) = stream.next() {
            results.set(
                &wasm_bindgen::JsValue::from(std::str::from_utf8(k).unwrap()),
                &wasm_bindgen::JsValue::from(v.to_string()),
            );
        }

        Ok(results)
    }

    pub fn levenstein_2_search(&mut self, query: &str) -> Result<js_sys::Map, JsError> {
        let levenstein = automaton::Levenshtein::new(query, 2)?;

        let stream = &mut self.map.search(levenstein).into_stream();

        let results = js_sys::Map::new();
        while let Some((k, v)) = stream.next() {
            results.set(
                &wasm_bindgen::JsValue::from(std::str::from_utf8(k).unwrap()),
                &wasm_bindgen::JsValue::from(v.to_string()),
            );
        }

        Ok(results)
    }

    // returns a unique identifier for the given element, if it appears in the dictionary
    pub fn locate(&mut self, query: &str) -> Result<js_sys::Array, JsError> {
        let automaton = automaton::Str::new(query);

        let stream = &mut self.map.search(automaton).into_stream();

        let results = js_sys::Array::new();
        while let Some((_k, v)) = stream.next() {
            results.push(&wasm_bindgen::JsValue::from(v.to_string()));
        }

        Ok(results)
    }

    pub fn extract(&mut self, value: u64) -> Result<js_sys::JsString, JsError> {
        let value = &mut self.map.as_fst().get_key(value).unwrap();
        let value_as_str = std::str::from_utf8(value).unwrap();

        Ok(js_sys::JsString::from(value_as_str))
    }

    pub fn get_triples_by_triple_pattern() {
        
    }
}

#[wasm_bindgen_test]
fn pass() {
    assert_eq!(1, 1);
}

// wasm-pack build --release --target web
// wasm-pack build --release --target nodejs

// wasm-pack test --node
