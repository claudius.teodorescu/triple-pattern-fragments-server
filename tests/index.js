import TpfServer from "../index.js";
import jsUntar from "https://cdn.jsdelivr.net/npm/js-untar@2.0.0/+esm";

await fetch(new URL("index.tar.gz", "https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/"), {
    mode: "cors",
  })
    .then(response => response.blob())
    .then(data => {
      const decompressionStream = data.stream().pipeThrough(new DecompressionStream('gzip'));
      return new Response(decompressionStream).arrayBuffer();
    })
    .then(jsUntar)
    .then(async files => {
      let filtered_files = files.filter(item => item.name !== "./");

      let tpfServer = await new TpfServer(filtered_files);

      console.time("search");
      let object_selector = "ceapă";
      let object_id = tpfServer.locate_objects(object_selector)[0];
      let id_triples_query_string = `.*;${object_id}`;
      let triple_ids = tpfServer._id_triples_searcher.regex_search(id_triples_query_string);

      triple_ids.forEach(triple_id => {
        let subject_id = triple_id.split(":")[0];

        let subject = tpfServer._subjects_searcher.extract(subject_id);
        console.log(`${object_selector}, ${subject}`);

      });
      console.timeEnd("search");
    });